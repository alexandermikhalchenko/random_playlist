package main.java.com.anahoret.random_playlist.ui;

import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.stage.DirectoryChooser;
import javafx.stage.Stage;

import java.io.File;

public class DirectoryPicker extends HBox {

    private static final String css = "/main/resources/ui/directory_picker/directory-picker.css";

    private final TextField textField;

    DirectoryPicker(String buttonText, String dialogTitle, final Stage stage) {
        setSpacing(10);
        textField = new TextField();
        textField.getStyleClass().add("file-path");
        textField.setEditable(false);

        Button btn = new Button(buttonText);
        btn.setOnAction(event -> {
            DirectoryChooser directoryChooser = new DirectoryChooser();
            directoryChooser.setTitle(dialogTitle);
            directoryChooser.setInitialDirectory(
                    new File(textField.getText().isEmpty() ? System.getProperty("user.home") : textField.getText())
            );
            File source = directoryChooser.showDialog(stage);
            if (source != null) textField.setText(source.getAbsolutePath());
        });

        getChildren().add(textField);
        getChildren().add(btn);

        getStylesheets().add(DirectoryPicker.class.getResource(css).toExternalForm());
    }

    public String getPath() {
        return textField.getText();
    }

    public void setPath(String path) {
        textField.setText(path);
    }
}
