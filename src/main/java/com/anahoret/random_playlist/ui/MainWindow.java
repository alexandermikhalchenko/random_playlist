package main.java.com.anahoret.random_playlist.ui;

import javafx.application.Application;
import javafx.beans.property.DoubleProperty;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import main.java.com.anahoret.random_playlist.playlist_builder.PlaylistBuilder;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.channels.FileChannel;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.CompletableFuture;

public class MainWindow extends Application {

    private static final String css = "/main/resources/ui/main_window/main-window.css";
    private DirectoryPicker sourcePicker;
    private DirectoryPicker destinationPicker;
    private TextField maxFilesField;

    @Override
    public void start(Stage primaryStage) throws Exception {
        GridPane grid = new GridPane();
        grid.setAlignment(Pos.TOP_CENTER);
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(25, 25, 25, 25));

        sourcePicker = new DirectoryPicker("Source...", "Source folder", primaryStage);
        grid.add(sourcePicker, 0, 0);

        destinationPicker = new DirectoryPicker("Destination...", "Destination folder", primaryStage);
        grid.add(destinationPicker, 0, 1);

        Button btnScan = new Button("Let it rock!");
        maxFilesField = new TextField("100");
        Text maxFilesLabel = new Text("Max files");
        maxFilesLabel.setId("max-files-label");
        HBox maxFilesBox = new HBox(5, btnScan, maxFilesLabel, maxFilesField);
        grid.add(maxFilesBox, 0, 2);

        VBox progressBox = new VBox();
        progressBox.setId("progress-box");
        progressBox.setFillWidth(true);
        ProgressBar progressBar = new ProgressBar(0);
        progressBar.setProgress(0);
        progressBar.prefWidthProperty().bind(progressBox.widthProperty().subtract(10));
        progressBox.getChildren().add(progressBar);
        grid.add(progressBox, 0, 3, 2, 1);

        Text statusField = new Text("Ready.");
        statusField.setId("action-target");
        statusField.getStyleClass().add("ready");
        grid.add(statusField, 0, 4, 2, 1);

        btnScan.setOnAction(event -> {
            try {
                statusField.setText("Creating playlist...");
                statusField.getStyleClass().remove("ready");
                statusField.getStyleClass().add("work-in-progress");
                int maxFiles = Integer.parseInt(maxFilesField.getText());

                List<File> playlist = PlaylistBuilder.build(sourcePicker.getPath(), maxFiles);

                CompletableFuture.runAsync(() -> {
                    copyFiles(playlist, destinationPicker.getPath(), progressBar.progressProperty());
                    statusField.setText("Done!");
                    statusField.getStyleClass().remove("work-in-progress");
                    statusField.getStyleClass().add("ready");
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        });

        loadPath();

        Scene scene = new Scene(grid, 800, 200);
        primaryStage.setScene(scene);
        scene.getStylesheets()
            .add(MainWindow.class.getResource(css).toExternalForm());

        primaryStage.setTitle("Random playlist");
        primaryStage.show();

        Runtime.getRuntime().addShutdownHook(new Thread(this::savePath));
    }

    private void loadPath() {
        Properties loadedProps = new Properties();
        File file = new File("./config.properties");
        if (file.exists()) {
            try (FileInputStream fis = new FileInputStream(file)) {
                loadedProps.load(fis);
            } catch (Exception e) {
                e.printStackTrace();
            }
            sourcePicker.setPath(loadedProps.getProperty("source"));
            destinationPicker.setPath(loadedProps.getProperty("destination"));
            maxFilesField.setText(loadedProps.getProperty("max_files"));
        }
    }

    private void savePath() {
        Properties configProps = new Properties();
        configProps.setProperty("source", sourcePicker.getPath());
        configProps.setProperty("destination", destinationPicker.getPath());
        configProps.setProperty("max_files", maxFilesField.getText());
        try (FileOutputStream fos = new FileOutputStream("./config.properties")) {
            configProps.store(fos, null);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void copyFiles(List<File> playlist, String destination, DoubleProperty progressProperty) {
        for (int i = 0; i < playlist.size(); i++) {
            File src = playlist.get(i);
            try (FileInputStream fis = new FileInputStream(src);
                 FileOutputStream fos = new FileOutputStream(destination + "/" + src.getName())) {
                FileChannel sourceChannel = fis.getChannel();
                FileChannel destChannel = fos.getChannel();
                destChannel.transferFrom(sourceChannel, 0, sourceChannel.size());
                progressProperty.set(i == playlist.size() - 1 ? 1 : (double)i / playlist.size());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public static void main(String[] args) {
        launch(args);
    }

}
