package main.java.com.anahoret.random_playlist.playlist_builder;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

public class PlaylistBuilder {

    public static List<File> build(String source, int maxTracks) throws Exception {
        List<File> mp3Files = findMp3Files(source);
        if (mp3Files.isEmpty()) return Collections.emptyList();
        return createPlaylist(mp3Files, maxTracks);
    }

    private static List<File> createPlaylist(List<File> mp3Files, int maxTracks) {
        Random rnd = new Random();
        List<File> playlist = new ArrayList<>(maxTracks);
        for (int i = 0; i < maxTracks; i++) {
            int index = rnd.nextInt(mp3Files.size());
            playlist.add(mp3Files.get(index));
        }
        return playlist;
    }

    private static List<File> findMp3Files(String source) throws IOException {
        return Files.find(
                Paths.get(source),
                Integer.MAX_VALUE,
                (path, basicFileAttributes) -> path.toString().endsWith(".mp3"))
                .map(Path::toFile)
                .collect(Collectors.toList());
    }

}
